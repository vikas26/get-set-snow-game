﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

public class PlayGameServices : MonoBehaviour {

	public static PlayGameServices instance;

	private string leaderboardId = "CgkIxOWK7boVEAIQAQ";

	void Awake() {
		if (instance) {
			DestroyImmediate (gameObject);
		} else {
			DontDestroyOnLoad(gameObject);
			instance = this;
		}

		// Google Play Games
		PlayGamesClientConfiguration config = 
			new PlayGamesClientConfiguration.Builder ()
				.Build ();
		PlayGamesPlatform.InitializeInstance(config);
		PlayGamesPlatform.DebugLogEnabled = true;
		PlayGamesPlatform.Activate();
	}

	private void SignInCallback(bool success) {
		if (success) {
			// Debug.Log ("Signed In: " + PlayGamesPlatform.Instance.localUser.userName);
			GameObject.FindGameObjectWithTag ("Canvas").transform.FindChild ("Leaderboard").gameObject.SetActive (true);
			GameObject.FindGameObjectWithTag ("Canvas").transform.FindChild ("TransLeaderboard").gameObject.SetActive (false);
			GameObject.FindGameObjectWithTag ("Canvas").transform.FindChild ("GooglePlayGames").gameObject.SetActive (false);
			GameObject.FindGameObjectWithTag ("Canvas").transform.FindChild ("TransGooglePlayGames").gameObject.SetActive (false);
			PlayerPrefs.SetString ("playGamesLoggedIn", "true");
		} else {
			Debug.Log ("Sign-in failed.");
			GameObject.FindGameObjectWithTag ("Canvas").transform.FindChild ("Leaderboard").gameObject.SetActive (false);
			GameObject.FindGameObjectWithTag ("Canvas").transform.FindChild ("TransLeaderboard").gameObject.SetActive (false);
			GameObject.FindGameObjectWithTag ("Canvas").transform.FindChild ("GooglePlayGames").gameObject.SetActive (true);
			GameObject.FindGameObjectWithTag ("Canvas").transform.FindChild ("TransGooglePlayGames").gameObject.SetActive (false);
			PlayerPrefs.SetString ("playGamesLoggedIn", "false");
		}
	}

	public void SignIn (bool silent) {
		GameObject.FindGameObjectWithTag ("Canvas").transform.FindChild ("Leaderboard").gameObject.SetActive (false);
		GameObject.FindGameObjectWithTag ("Canvas").transform.FindChild ("TransLeaderboard").gameObject.SetActive (false);
		GameObject.FindGameObjectWithTag ("Canvas").transform.FindChild ("GooglePlayGames").gameObject.SetActive (false);
		GameObject.FindGameObjectWithTag ("Canvas").transform.FindChild ("TransGooglePlayGames").gameObject.SetActive (true);
		PlayGamesPlatform.Instance.Authenticate(SignInCallback, silent);
	}

	public void SignOut() {
		PlayGamesPlatform.Instance.SignOut();
	}

	public void PostScoreToLeaderboard (int score) {
		Social.ReportScore (score, leaderboardId, PostScoreCallback);
	}

	private void PostScoreCallback (bool success) {
		// Debug.Log ("posted : " + success.ToString ());
	}

	public void ShowLeaderBoard () {
		PlayGamesPlatform.Instance.ShowLeaderboardUI (leaderboardId);
	}
}