﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveItself : MonoBehaviour {

	// movement
	public bool movementFlag = false;

	public enum movMode { horizontal, vertical };
	public movMode movementDirectionEnum;

	public bool movePositive = true;
	int stepDirection = 1;
	public float stepSize = 0.0f;
	public float distanceToTravel = 0f;

	// rotation
	public bool rotationFlag = false;
	public bool rotateClockwise = true;
	int rotationDirection = -1;
	public float stepRotation = 2f;
	public float maxRotation = 40f;
	// problem is scorer rotation

	Vector3 initialPosition;
	Vector3 newPosition;

	Vector3 initialRotation;
	Vector3 newRotation;

	// initial position flag for AI power
	private bool initialPositionFlag = false;

	// Use this for initialization
	void Start () {
		initialPosition = transform.localPosition;
		initialRotation = transform.localEulerAngles;

		if (!movePositive) {
			stepDirection = -1;
		}
		if (!rotateClockwise) {
			rotationDirection = 1;
		}

		if (!movementFlag && !rotationFlag) {
			Destroy (this);
		}
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (PlayerPrefs.GetString ("gamePaused") != "true" && !initialPositionFlag) {
			if (movementFlag) {
				newPosition = transform.localPosition;
				switch (movementDirectionEnum.ToString ()) {
				case "horizontal":
					newPosition.x = GetNewPositionCoordinate (initialPosition.x, newPosition.x);
					break;
				case "vertical":
					newPosition.y = GetNewPositionCoordinate (initialPosition.y, newPosition.y);
					break;
				}
				transform.localPosition = newPosition;
			} else if (rotationFlag) {
				newRotation = transform.localEulerAngles;
				newRotation.z = GetNewRotationalCoordinate (initialRotation.z, newRotation.z);
				transform.localEulerAngles = newRotation;
				if (transform.FindChild ("BlockerScore")) {
					newRotation.z *= -1;
					transform.FindChild ("BlockerScore").localEulerAngles = newRotation;
				} else if (transform.FindChild ("DestroyerScore")) {
					newRotation.z *= -1;
					transform.FindChild ("DestroyerScore").localEulerAngles = newRotation;
				}
			}
		}
	}

	// translation new coordinate
	float GetNewPositionCoordinate (float baseValue, float movementCoordinate) {
		float newCoordinate = movementCoordinate;
		if (stepDirection == 1) {
			// +ve side
			if (baseValue + distanceToTravel > movementCoordinate) {
				newCoordinate += stepSize;
			} else {
				newCoordinate -= stepSize;
				stepDirection *= -1;
			}
		} else {
			if (baseValue - distanceToTravel <= movementCoordinate) {
				newCoordinate -= stepSize;
			} else {
				newCoordinate += stepSize;
				stepDirection *= -1;
			}
		}
		return newCoordinate;
	}

	// rotational new coordinate
	float GetNewRotationalCoordinate (float baseValue, float movementCoordinate) {
		float newCoordinate = movementCoordinate;
		if (movementCoordinate > 180) {
			movementCoordinate -= 360;
		}
		if (rotationDirection == 1) {
			// anti clockwise
			if (baseValue + maxRotation > movementCoordinate) {
				newCoordinate += stepRotation;
			} else {
				newCoordinate -= stepRotation;
				rotationDirection *= -1;
			}
		} else {
			// clockwise
			if (baseValue - maxRotation <= movementCoordinate) {
				newCoordinate -= stepRotation;
			} else {
				newCoordinate += stepRotation;
				rotationDirection *= -1;
			}
		}
		return newCoordinate;
	}

	// initial state
	public void TakeToInitialState () {
		initialPositionFlag = true;
		gameObject.transform.localPosition = initialPosition;
		gameObject.transform.localEulerAngles = initialRotation;
		if (gameObject.transform.Find ("BlockerScore")) {
			gameObject.transform.Find ("BlockerScore").localEulerAngles = Vector3.zero;
		}
		if (gameObject.transform.Find ("DestroyerScore")) {
			gameObject.transform.Find ("DestroyerScore").localEulerAngles = Vector3.zero;
		}
	}

	// allow movement
	public void AllowMovement () {
		initialPositionFlag = false;
	}
}
