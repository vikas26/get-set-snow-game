﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUIRenderer : MonoBehaviour {

	GameObject myCanvas;
	GameObject player;
	private IEnumerator powerCoroutine;

	// Use this for initialization
	void Awake () {
		myCanvas = GameObject.FindGameObjectWithTag("Canvas");
		player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		UpdateScore ();
	}

	// Update score
	void UpdateScore () {
		myCanvas.transform.FindChild ("HighestScore").GetComponent<Text>().text = PlayerPrefs.GetInt ("highestScore").ToString ();
		myCanvas.transform.FindChild ("CurrentScore").GetComponent<Text>().text = PlayerPrefs.GetInt ("currentScore").ToString ();
	}

	// Game over
	public void GameOver () {
		GameObject gameOverPanel = myCanvas.transform.FindChild ("GameOverPanel").gameObject;

		gameOverPanel.transform.FindChild("YourScore").GetComponent<Text>().text = PlayerPrefs.GetInt ("currentScore").ToString();
		if (PlayerPrefs.GetString ("scoredHighestScoreFlag") == "true") {
			PlayerPrefs.SetString ("scoredHighestScoreFlag", "false");
			gameOverPanel.transform.FindChild ("HighestScoreLabel").gameObject.SetActive (true);
			gameOverPanel.transform.FindChild ("NormalScoreLabel").gameObject.SetActive (false);
		} else {
			gameOverPanel.transform.FindChild ("HighestScoreLabel").gameObject.SetActive (false);
			gameOverPanel.transform.FindChild ("NormalScoreLabel").gameObject.SetActive (true);
		}
		gameOverPanel.gameObject.SetActive (true);
	}

	// Pause game
	public void PauseGame () {
		myCanvas.transform.FindChild ("PausePanel").gameObject.SetActive (true);
		if (PlayerPrefs.GetString ("powerCountdown") == "true") {
			PlayerPrefs.SetString ("pausePowerCountdown", "true");
		}
	}

	// Resume game
	public void ResumeGame () {
		myCanvas.transform.FindChild ("PausePanel").gameObject.SetActive (false);
		StartCoroutine (Countdown (3, 1, 1f, true));
	}

	// Restart game
	public void RestartGame () {
		player.SetActive (false);
		myCanvas.transform.FindChild ("GameOverPanel").gameObject.SetActive (false);
		myCanvas.transform.FindChild ("PausePanel").gameObject.SetActive (false);
		myCanvas.transform.FindChild ("ComingSoonPanel").gameObject.SetActive (false);
		StartCoroutine (MoveCameraSmoothlyToPosition (2f));
	}

	// Countdown
	IEnumerator Countdown (int startFrom, int endAt, float timeForOne, bool resumeGame = false) {
		myCanvas.transform.FindChild ("CountdownPanel").gameObject.SetActive (true);
		int i = startFrom;
		while (true) {
			myCanvas.transform.FindChild ("CountdownPanel").FindChild ("CountdownText").GetComponent<Text> ().text = i.ToString ();
			myCanvas.transform.FindChild ("CountdownPanel").FindChild ("CountdownText").gameObject.GetComponent<Animation> ().Stop ();
			myCanvas.transform.FindChild ("CountdownPanel").FindChild ("CountdownText").gameObject.GetComponent<Animation> ().Play ();
			yield return new WaitForSeconds (1.1f);
			if (i == endAt) {
				break;
			}
			i -= 1;
		}
		myCanvas.transform.FindChild ("CountdownPanel").gameObject.SetActive (false);
		myCanvas.transform.FindChild ("CountdownPanel").FindChild ("CountdownText").gameObject.GetComponent<Animation> ().Stop ();
		if (PlayerPrefs.GetString ("powerCountdown") == "true") {
			PlayerPrefs.SetString ("pausePowerCountdown", "false");
		}
		if (resumeGame) {
			player.GetComponent<Rigidbody> ().drag = PlayerPrefs.GetFloat ("playerInitialDrag");
			player.GetComponent<Rigidbody> ().useGravity = false;
			player.GetComponent<Rigidbody> ().useGravity = true;
		}
		PlayerPrefs.SetString ("gamePaused", "false");
	}

	// Camera smooth movement on restart
	IEnumerator MoveCameraSmoothlyToPosition (float transitionTime) {
		string[] pSplitString = PlayerPrefs.GetString ("playerInitialPosition").Split (";" [0]);
		string[] cSplitString = PlayerPrefs.GetString ("cameraInitialPosition").Split (";" [0]);

		Vector3 playerInitialPosition = new Vector3 (float.Parse (pSplitString [0]), float.Parse (pSplitString [1]), float.Parse (pSplitString [2]));
		Vector3 cameraInitialPosition = new Vector3 (float.Parse (cSplitString [0]), float.Parse (cSplitString [1]), float.Parse (cSplitString [2]));
		Vector3 currentCameraPosition = Camera.main.transform.position;

		float startTime = Time.time;
		float endTime = Time.time + transitionTime;
		float currentProportion = 0f;

		while (true) {
			currentProportion = (Time.time - startTime) / (endTime - startTime);
			Camera.main.transform.position = Vector3.Lerp (currentCameraPosition, cameraInitialPosition, currentProportion);
			if (currentProportion >= 1) {
				break;
			}
			yield return new WaitForSeconds (0.01f);
		}
		player.transform.position = playerInitialPosition;

		// hide pupil
		EyeMovementTowardsEnemy[] eyes = player.GetComponentsInChildren<EyeMovementTowardsEnemy> ();
		foreach (EyeMovementTowardsEnemy eye in eyes) {
			eye.gameObject.SetActive (false);
		}

		player.SetActive (true);
		player.GetComponent<Animation> ().Play ("ShowBall");
		yield return new WaitForSeconds (1f);

		// set pupil to initial position
		foreach (EyeMovementTowardsEnemy eye in eyes) {
			eye.gameObject.transform.localPosition = new Vector3 (0f, 0f, -2f);
			eye.gameObject.SetActive (true);
		}
		PlayerPrefs.SetString ("gamePaused", "false");
	}

	// Hiding object smoothly - NOT TESTED
	IEnumerator SmoothHide (GameObject thisObject, float transitionTime) {
		float startTime = Time.time;
		float endTime = Time.time + transitionTime;
		float currentProportion = 0f;

		Vector3 finalScale = new Vector3 (0, 0, 0);
		Vector3 initialScale = thisObject.transform.localScale;

		while (true) {
			currentProportion = (Time.time - startTime) / (endTime - startTime);
			if (currentProportion > 1) {
				currentProportion = 1;
			}
			thisObject.transform.localScale = Vector3.Lerp (initialScale, finalScale, currentProportion);
			if (currentProportion == 1) {
				break;
			}
			yield return new WaitForSeconds (0.01f);
		}
	}

	// power countdown
	public void PowerCountdown (int startFrom, int endAt) {
		myCanvas.transform.FindChild ("PausePanel").gameObject.SetActive (false);
		string powerCountdownPanel = "PowerCountdownPanel";
		PlayerPrefs.SetString ("powerCountdown", "true");
		powerCoroutine = PowerCountdownCoroutine (startFrom, endAt, 1f, powerCountdownPanel);
		StartCoroutine (powerCoroutine);
	}

	// Stop power countdown
	public void StopPowerCountdown () {
		StopCoroutine (powerCoroutine);
		string powerCountdownPanel = "PowerCountdownPanel";
		HidePowerCountdown (powerCountdownPanel);
	}

	// Hide power countdown
	void HidePowerCountdown (string powerCountdownPanel) {
		PlayerPrefs.SetString ("powerCountdown", "false");
		myCanvas.transform.FindChild (powerCountdownPanel).gameObject.SetActive (false);
		myCanvas.transform.FindChild (powerCountdownPanel).FindChild ("CountdownText").gameObject.GetComponent<Animation> ().Stop ();
		gameObject.GetComponent<Manager>().BallPower (1, 0, false, true);
	}

	// power countdown
	IEnumerator PowerCountdownCoroutine (int startFrom, int endAt, float timeForOne, string powerCountdownPanel) {
		myCanvas.transform.FindChild (powerCountdownPanel).gameObject.SetActive (true);
		int i = startFrom;
		while (true) {
			if (PlayerPrefs.GetString ("pausePowerCountdown") == "true") {
				yield return new WaitForSeconds (0.01f);
			} else {
				myCanvas.transform.FindChild (powerCountdownPanel).FindChild ("CountdownText").GetComponent<Text> ().text = i.ToString ();
				myCanvas.transform.FindChild (powerCountdownPanel).FindChild ("CountdownText").gameObject.GetComponent<Animation> ().Stop ();
				myCanvas.transform.FindChild (powerCountdownPanel).FindChild ("CountdownText").gameObject.GetComponent<Animation> ().Play ();
				yield return new WaitForSeconds (1.1f);
				if (i == endAt) {
					break;
				}
				i -= 1;
			}
		}
		HidePowerCountdown (powerCountdownPanel);
	}

	// show level text
	public void ShowLevel (int levelNumber) {
		string levelPanel = "LevelChangePanel";
		GameObject levelChangeObject = myCanvas.transform.FindChild (levelPanel).gameObject;
		levelChangeObject.transform.FindChild ("Text").GetComponent<Text> ().text = "Level " + levelNumber.ToString ();
		StartCoroutine (ShowLevelCoroutine (levelChangeObject));
	}

	// show level number animation
	IEnumerator ShowLevelCoroutine (GameObject levelChangeObject) {
		levelChangeObject.SetActive (true);
		levelChangeObject.transform.FindChild ("Text").GetComponent<Animation> ().Play ("LevelChange");
		yield return new WaitForSeconds (2f);
		levelChangeObject.SetActive (false);
	}

	// Show Power huielines
	public void ShowPowerInstructions () {
		PlayerPrefs.SetString ("powerInstructions", "true");
		myCanvas.transform.FindChild ("PowerPanel").gameObject.SetActive (true);
	}

	// hide instructions
	public void HidePowerInstructions () {
		PlayerPrefs.SetString ("powerInstructions", "false");
		myCanvas.transform.FindChild ("PowerPanel").gameObject.SetActive (false);
	}

	// show instructions
	public void ShowGameInstructions () {
		PlayerPrefs.SetString ("touchGameInstructions", "true");
		myCanvas.transform.FindChild ("GameGuidelinesTouchPanel").gameObject.SetActive (true);
	}

	// hide instructions
	public void HideGameInstructions () {
		PlayerPrefs.SetString ("touchGameInstructions", "false");
		myCanvas.transform.FindChild ("GameGuidelinesTouchPanel").gameObject.SetActive (false);
	}

	// show coming soon panel
	public void ShowComingSoon () {
		myCanvas.transform.FindChild ("ComingSoonPanel").gameObject.SetActive (true);
	}

	// rate us popup
	public void ShowRateUs () {
		myCanvas.transform.FindChild ("RateUsPanel").gameObject.SetActive (true);
	}

	// show coming soon panel
	public void HideRateUs () {
		myCanvas.transform.FindChild ("RateUsPanel").gameObject.SetActive (false);
	}

	// show ai countdown
	public void ShowAIDisplay (int restrictorsLeft, bool updateFlag = false) {
		if (!updateFlag) {
			myCanvas.transform.FindChild ("AICountdownPanel").gameObject.SetActive (true);
		}
		myCanvas.transform.FindChild ("AICountdownPanel").FindChild ("CountdownText").GetComponent<Animation> ().Play ();
		myCanvas.transform.FindChild ("AICountdownPanel").FindChild ("CountdownText").GetComponent<Text> ().text = restrictorsLeft.ToString ();
	}

	// hide ai countdown
	public void HideAIDisplay () {
		StartCoroutine (HideAIDisplayDelayed ());
	}

	// hide ai countdown after delay
	IEnumerator HideAIDisplayDelayed () {
		yield return new WaitForSeconds (0.5f);
		myCanvas.transform.FindChild ("AICountdownPanel").gameObject.SetActive (false);
		string powerCountdownPanel = "PowerCountdownPanel";
		HidePowerCountdown (powerCountdownPanel);
	}
}
