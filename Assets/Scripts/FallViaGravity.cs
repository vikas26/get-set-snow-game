﻿using UnityEngine;
using System.Collections;

public class FallViaGravity : MonoBehaviour {

	// player - balls
	GameObject player;

	// manoeuvre vars
	public float yStepSize;
	public float yMaxBallStepSize;

	// temporary vars to change positions
	Vector3 playerTransform;
	Vector3 cameraTransform;

	// Use this for initialization
	void Start () {
		// player gameobject
		player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		// if game not paused then ball will fall
		if (PlayerPrefs.GetString ("gamePaused") != "true") {
			// current player and camera positions
			playerTransform = player.transform.position;
			cameraTransform = Camera.main.transform.position;

			// change and update positions of camera and player as they are falling
			float myCurrentYPos = Camera.main.WorldToScreenPoint (new Vector3 (0, player.transform.localPosition.y, 0)).y;
			float maxBallSpeedPercentage = ((myCurrentYPos - Screen.height / 2) / (Screen.height / 2)) * 100;

			playerTransform.y -= ((maxBallSpeedPercentage / 100f) * yMaxBallStepSize) + (((100f - maxBallSpeedPercentage) / 100f) * yStepSize);
			cameraTransform.y -= yStepSize;

			player.transform.position = playerTransform;
			Camera.main.transform.position = cameraTransform;
		}
	}
}
