﻿using UnityEngine;
using System.Collections;

public class BackgroundColorTransition : MonoBehaviour {

	Color32 lastBgColor;
	Color32 lerpedBgColor;

	// Change lebel to change color
	public void LevelChange (Color32 newColor, float transitionTime) {
		StartCoroutine (ChangeColor (newColor, transitionTime)); 
	}

	// Changes color smoothly
	IEnumerator ChangeColor (Color32 newColor, float transitionTime) {
		lastBgColor = GameObject.FindGameObjectWithTag ("Background").GetComponent<MeshRenderer> ().material.color;

		float startingTime = Time.time;
		float timeProportion = 0f;

		while (true) {
			if (transitionTime < 0.01) {
				timeProportion = 1;
			} else {
				timeProportion = (Time.time - startingTime) / transitionTime;
			}

			lerpedBgColor = Color.Lerp (lastBgColor, newColor, timeProportion);
			GameObject.FindGameObjectWithTag ("Background").GetComponent<MeshRenderer>().material.color = lerpedBgColor;

			if (timeProportion >= 1) {
				break;
			}
			yield return new WaitForSeconds (0.001f);
		}
	}
}
