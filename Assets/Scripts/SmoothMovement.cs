﻿using UnityEngine;
using System.Collections;

public class SmoothMovement : MonoBehaviour {

	public Transform player;
	public float delayFactor = 1f;

	public bool horizontalMovementFlag = true;
	public bool verticalMovementFlag = true;

	public float heightDistance = 2f;

	Vector3 newPosition;
	Vector3 playerPosition;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		playerPosition = player.position;
		playerPosition.y -= heightDistance;

		newPosition = Vector3.Lerp (transform.position, playerPosition, Time.deltaTime * delayFactor);
		newPosition.z = transform.position.z;
		if (!horizontalMovementFlag) {
			newPosition.x = transform.position.x;
		}
		if (!verticalMovementFlag) {
			newPosition.y = transform.position.y;
		}
		transform.position = newPosition;
	}
}
