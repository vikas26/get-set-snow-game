﻿using UnityEngine;
using System.Collections;

public class Manoeuvring : MonoBehaviour {

	public float xMovementStepSize;
	public float xMovementSpeed;

	public float forceMultiplier = 1000.0f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		bool leftKey = Input.GetKey (KeyCode.LeftArrow);
		bool rightKey = Input.GetKey (KeyCode.RightArrow);
		switch (PlayerPrefs.GetInt ("manoeuvringWay")) {
		case 1:
			// tilt
			Vector3 movement = new Vector3 (Input.acceleration.x, 0.0f, 0.0f);
			if (Mathf.Abs (movement.x) > float.Parse (PlayerPrefs.GetString ("tiltSensitivity"))) {
				if (movement.x < 0) {
					// left
					leftKey = true;
					rightKey = false;
				} else {
					// right
					leftKey = false;
					rightKey = true;
				}
			}
			break;
		case 2:
			// touch manoeuvring
			switch (Input.touchCount) {
			case 1:
				// one screen touch
				float oneX1 = Input.GetTouch (0).rawPosition.x;
				float oneY1 = Input.GetTouch (0).rawPosition.y;
				if (oneY1 < Screen.height / 2) {
					if (oneX1 < Screen.width / 2) {
						leftKey = true;
						rightKey = false;
					} else {
						leftKey = false;
						rightKey = true;
					}
				}
				break;
			case 2:
				// two screen touches
				float twoX1 = Input.GetTouch (0).rawPosition.x;
				float twoY1 = Input.GetTouch (0).rawPosition.y;
				float twoX2 = Input.GetTouch (1).rawPosition.x;
				float twoY2 = Input.GetTouch (1).rawPosition.y;
				bool twoLeftTouch = false;
				bool twoRightTouch = false;

				if (twoY1 < Screen.height / 2) {
					if (twoX1 < Screen.width / 2) {
						twoLeftTouch = true;
					} else {
						twoRightTouch = true;
					}
				}

				if (twoY2 < Screen.height / 2) {
					if (twoX2 < Screen.width / 2) {
						if (!twoRightTouch) {
							twoLeftTouch = true;
						} else {
							twoLeftTouch = false;
							twoRightTouch = false;
						}
					} else {
						if (!twoLeftTouch) {
							twoRightTouch = true;
						} else {
							twoLeftTouch = false;
							twoRightTouch = false;
						}
					}
				}

				leftKey = twoLeftTouch;
				rightKey = twoRightTouch;
				break;
			}
			break;
		}

		// player current position
		Vector3 playerPosition = gameObject.transform.position;
		Vector3 playerForce = new Vector3 (0f, 0f, 0f);

		// movement
		if (leftKey && rightKey) {
		} else if (leftKey) {
			playerPosition.x -= xMovementStepSize;
			playerForce.x = -1f;
		} else if (rightKey) {
			playerPosition.x += xMovementStepSize;
			playerForce.x = 1f;
		}

		// if game not paused then ball will fall
		if (PlayerPrefs.GetString ("gamePaused") != "true" && PlayerPrefs.GetInt ("ballType") != 4) {
			gameObject.GetComponent<Rigidbody> ().AddForce (playerForce * forceMultiplier);
			// gameObject.transform.position = Vector3.Lerp (gameObject.transform.position, playerPosition, Time.deltaTime * xMovementSpeed);
		}
	}
}
