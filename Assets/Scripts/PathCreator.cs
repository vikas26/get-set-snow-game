﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathCreator : MonoBehaviour {

	// boundaries
	private float minX = -3.0f;
	private float maxX = 3.0f;
	private float minY = 2.5f;
	private float maxY = 4f;
	private int mergeYInLevels = 10;

	// translation
	private float minTranslationSpeed = 0.01f;
	private float maxTranslationSpeed = 0.03f;
	private float minTranslationDist = 0.5f;
	private float maxTranslationDist = 1.0f;

	// rotation
	private float minRotationSpeed = 0.5f;
	private float maxRotationSpeed = 2f;
	private float minRotationAngle = 30f;
	private float maxRotationAngle = 60f;

	// powers
	public GameObject[] powers;
	public GameObject powerParentObject;
	private int afterRestrictor = 2;
	private int beforeRestrictor = 9;
	private int afterRestrictorForAI = 7;
	private int beforeRestrictorForAI = 9;

	private float powerXMargin = 0.7f;
	private float powerYMargin = 1f;

	// blockers
	public GameObject blocker2;

	// destroyers
	public GameObject destroyer2;

	// level wise properties
	public class Level {
		public bool bTranslation { get; set; }
		public bool bRotation { get; set; }
		public bool dTranslation { get; set; }
		public bool dRotation { get; set; }
	}
	List<Level> levelList = new List<Level>();

	// level wise gaps
	private float initialYGap = 9f;
	private float finalYGap = 5f;

	// restrictors count
	public GameObject restrictorObject;
	private GameObject blockerParentObject;
	private GameObject destroyerParentObject;
	private int blockersCount = 5;
	private int destroyersCount = 5;
	private List<int> restrictorsList = new List<int> ();
	private List<int> restrictorsSizeList = new List<int> (new int[] { 2, 3, 4 });

	// random number instance
	System.Random rd = new System.Random ();

	// last bar side
	private bool lastSlabOnLeft = true;

	// Use this for initialization
	void Start () {
		blockerParentObject = restrictorObject.transform.Find ("Blockers").gameObject;
		destroyerParentObject = restrictorObject.transform.Find ("Destroyers").gameObject;

		AddLevelList ();
	}

	// creates whole level (restrictors, powers, level changers
	public void CreateWholeLevel (int levelToCreate, GameObject colliderObject) {
		CreateLevel (levelToCreate, colliderObject.transform.localPosition);
		LevelYOffset (colliderObject.transform.localPosition.y);

		restrictorsList.Clear ();
		restrictorsList.AddRange (System.Linq.Enumerable.Repeat (0, blockersCount));
		restrictorsList.AddRange (System.Linq.Enumerable.Repeat (1, destroyersCount));

		float atleastYPos = initialYGap;
		bool generateMapFlag = false;
		GameObject powerObject = CreatePower (levelToCreate);
		int powerAfter = powerObject.name.Contains ("AIPower") ? rd.Next (afterRestrictorForAI, beforeRestrictorForAI) : rd.Next (afterRestrictor, beforeRestrictor);

		for (int i = 0; i < blockersCount + destroyersCount; i++) {
			if (i == powerAfter) {
				PowerPosition (powerObject, atleastYPos, powerAfter);
				generateMapFlag = powerObject.name.Contains ("AIPower") ? true : false;
			}
			GameObject newRestrictorObject = CreateRestrictor (levelToCreate, atleastYPos);
			atleastYPos = -1 * newRestrictorObject.transform.localPosition.y;

			if (generateMapFlag) {
				// generate map
				GenerateMap (newRestrictorObject);
			}
		}
		if (generateMapFlag) {
			gameObject.GetComponent<GenerateMap> ().FindPath ();
		}

		atleastYPos += (-1 * colliderObject.transform.localPosition.y);
		ShiftLevelIncrementor (colliderObject, atleastYPos);
	}

	// create level
	void CreateLevel (int levelToCreate, Vector3 colliderPosition) {
		// create level under restrictor (Blockers and destroyers)
		for (int i = 0; i < 2; i++) {
			GameObject levelObject = new GameObject ("Level "+levelToCreate.ToString());
			levelObject.transform.localPosition = colliderPosition;
			if (i == 0) {
				levelObject.transform.parent = blockerParentObject.transform;
			} else {
				levelObject.transform.parent = destroyerParentObject.transform;
			}
		}
		GameObject powerLevelObject = new GameObject ("Level "+levelToCreate.ToString());
		powerLevelObject.transform.parent = powerParentObject.transform;
		powerLevelObject.transform.localPosition = colliderPosition;
	}

	// create restrictor
	GameObject CreateRestrictor  (int levelToCreate, float atleastYPos) {
		GameObject newRestrictorObject;
		Vector3 newPosition;
		int restrictorTypeIndex = rd.Next (0, restrictorsList.Count);
		int restrictorsSizeIndex = rd.Next (0, restrictorsSizeList.Count);
		bool blockerType = true;

		if (restrictorsList [restrictorTypeIndex] == 0) {
			// blocker
			newRestrictorObject = Instantiate (blocker2, blockerParentObject.transform.GetChild(levelToCreate - 1).transform) as GameObject;
		} else {
			// destroyer
			blockerType = false;
			newRestrictorObject = Instantiate (destroyer2, destroyerParentObject.transform.GetChild(levelToCreate - 1).transform) as GameObject;
		}

		List<int> slabSide = new List<int> ();
		slabSide.Add (1);
		slabSide.Add (2);
		slabSide.Add (lastSlabOnLeft ? 2 : 1);

		newPosition = newRestrictorObject.transform.localPosition;

		float slab4Adjustment = restrictorsSizeList [restrictorsSizeIndex] == 4 ? 0.5f : 0f;
		if (slabSide [rd.Next (0, slabSide.Count)] == 1) {
			// on left
			newPosition.x = GetRandomNumber (minX + 1f - slab4Adjustment, -1 * slab4Adjustment);
			lastSlabOnLeft = true;
		} else {
			newPosition.x = GetRandomNumber (slab4Adjustment, maxX - 1f + slab4Adjustment);
			lastSlabOnLeft = false;
		}

		float minTemp = minY;
		float maxTemp = maxY - (((maxY - minY) * levelToCreate) / mergeYInLevels);
		if (minTemp >= maxTemp) {
			minTemp += 0.01f;
			float temp = minTemp;
			minTemp = maxTemp;
			maxTemp = temp;
		}
		newPosition.y = -1 * (GetRandomNumber (minTemp, maxTemp) + atleastYPos);
		newRestrictorObject.transform.localPosition = newPosition;

		newRestrictorObject = ChangeRestrictorsTo (newRestrictorObject, restrictorsSizeList[restrictorsSizeIndex]);
		newRestrictorObject = AddMovementToRestrictors (levelToCreate, newRestrictorObject, blockerType);
		restrictorsList.RemoveAt (restrictorTypeIndex);

		return newRestrictorObject;
	}

	// tranform restrictors into different sizes
	GameObject ChangeRestrictorsTo (GameObject restrictor, int sizeNumber) {
		// body scale
		Vector3 bodyScale = restrictor.transform.Find ("Body").transform.localScale;
		bodyScale.x = sizeNumber;
		restrictor.transform.Find ("Body").transform.localScale = bodyScale;
	
		// eye position
		Vector3 eyePos = restrictor.transform.Find ("Body").transform.localPosition;
		eyePos.x = (0.5f) * (sizeNumber - 1f) - 0.05f;
		if (restrictor.transform.localPosition.x > 0) {
			eyePos.x *= -1f;
		} else {
			restrictor.transform.Find ("Eyes/EyeBall/EyeLashes").GetComponent<SpriteRenderer> ().flipX = !restrictor.transform.Find ("Eyes/EyeBall/EyeLashes").GetComponent<SpriteRenderer> ().flipX;
			Vector3 newPosition = restrictor.transform.Find ("Eyes/EyeBall/EyeLashes").transform.localPosition;
			newPosition.x *= -1;
			restrictor.transform.Find ("Eyes/EyeBall/EyeLashes").transform.localPosition = newPosition;
			Vector3 newRotation = restrictor.transform.Find ("Eyes/EyeBall/EyeLashes").transform.localEulerAngles;
			newRotation.z *= -1;
			restrictor.transform.Find ("Eyes/EyeBall/EyeLashes").transform.localEulerAngles = newRotation;
		}
		restrictor.transform.Find ("Eyes").transform.localPosition = eyePos;

		return restrictor;
	}

	// creates new level changer
	void ShiftLevelIncrementor (GameObject colliderObject, float atleastYPos) {
		GameObject newLevelchanger = Instantiate (colliderObject, colliderObject.transform.parent) as GameObject;
		colliderObject.GetComponent<LevelChanger> ().GenerateNextLevel = false;
		newLevelchanger.GetComponent<LevelChanger> ().GenerateNextLevel = true;
		newLevelchanger.GetComponent<LevelChanger> ().ChangeLevelTo += 1;

		Vector3 newPosition = colliderObject.transform.localPosition;
		newPosition.y = -1 * (atleastYPos + finalYGap);
		newLevelchanger.transform.localPosition = newPosition;
	}

	// specify moveitself params for restrictors
	GameObject AddMovementToRestrictors (int levelToCreate, GameObject newRestrictorObject, bool blockerType) {
		int boundedLevelNumber = levelToCreate > levelList.Count ? levelList.Count : levelToCreate;
		List<int> movements = new List<int> ();
		movements.Add (0);

		float movementEasinessLevel = 3f;
		if (blockerType) {
			if (levelList [boundedLevelNumber - 1].bTranslation) {
				movements.AddRange (System.Linq.Enumerable.Repeat (1, (int)Mathf.Ceil (levelToCreate / movementEasinessLevel)));
			}
			if (levelList [boundedLevelNumber - 1].bRotation) {
				movements.AddRange (System.Linq.Enumerable.Repeat (2, (int)Mathf.Ceil (levelToCreate / movementEasinessLevel)));
			}
		} else {
			if (levelList [boundedLevelNumber - 1].dTranslation) {
				movements.AddRange (System.Linq.Enumerable.Repeat (1, (int)Mathf.Ceil (levelToCreate / movementEasinessLevel)));
			}
			if (levelList [boundedLevelNumber - 1].dRotation) {
				movements.AddRange (System.Linq.Enumerable.Repeat (2, (int)Mathf.Ceil (levelToCreate / movementEasinessLevel)));
			}
		}

		float distOrAngle = (float) rd.NextDouble ();
		float speed = (float) rd.NextDouble ();
		bool positiveOrClockwise = rd.Next (0, 2) == 0 ? false : true;

		switch (movements[rd.Next (0, movements.Count)]) {
		case 0:
			// no movement
			newRestrictorObject.GetComponent<MoveItself> ().movementFlag = false;
			newRestrictorObject.GetComponent<MoveItself> ().rotationFlag = false;
			break;
		case 1:
			// translation
			newRestrictorObject.GetComponent<MoveItself> ().movementFlag = true;
			newRestrictorObject.GetComponent<MoveItself> ().rotationFlag = false;
			newRestrictorObject.GetComponent<MoveItself> ().stepSize = minTranslationSpeed * speed + maxTranslationSpeed * (1 - speed);
			newRestrictorObject.GetComponent<MoveItself> ().distanceToTravel = minTranslationDist * distOrAngle + maxTranslationDist * (1 - distOrAngle);
			newRestrictorObject.GetComponent<MoveItself> ().movePositive = positiveOrClockwise;
			break;
		case 2:
			// rotation
			newRestrictorObject.GetComponent<MoveItself> ().movementFlag = false;
			newRestrictorObject.GetComponent<MoveItself> ().rotationFlag = true;
			newRestrictorObject.GetComponent<MoveItself> ().stepRotation = minRotationSpeed * speed + maxRotationSpeed * (1 - speed);
			newRestrictorObject.GetComponent<MoveItself> ().maxRotation = minRotationAngle * distOrAngle + maxRotationAngle * (1 - distOrAngle);
			newRestrictorObject.GetComponent<MoveItself> ().rotateClockwise = positiveOrClockwise;
			break;
		}

		return newRestrictorObject;
	}

	// creates power
	GameObject CreatePower (int levelToCreate) {
		GameObject newPowerObject = Instantiate (powers [(levelToCreate - 1) % powers.Length], powerParentObject.transform.GetChild(levelToCreate - 1).transform) as GameObject;
		return newPowerObject;
	}

	// position of power
	void PowerPosition (GameObject newPowerObject, float atleastYPos, int powerAfter) {
		Vector3 newPosition = newPowerObject.transform.localPosition;
		newPosition.x = GetRandomNumber (minX + powerXMargin, maxX - powerXMargin);
		newPosition.y = -1 * (GetRandomNumber (0 + powerYMargin, minY - powerYMargin) + atleastYPos);
		newPowerObject.transform.localPosition = newPosition;

		if (newPowerObject.name.Contains ("AIPower")) {
			newPowerObject.GetComponent<PowerColliderVars> ().powerTime = blockersCount + destroyersCount - powerAfter;
			PowerLocalPosition (newPowerObject);
		}
	}

	// generate random double
	private float GetRandomNumber(float minimum, float maximum) { 
		return ((float) rd.NextDouble ()) * (maximum - minimum) + minimum;
	}

	// levels properties
	void AddLevelList () {
		// level 1
		levelList.Add ( new Level {
			bTranslation = false,
			bRotation = false,
			dTranslation = false,
			dRotation = false
		});
		// level 2
		levelList.Add ( new Level {
			bTranslation = true,
			bRotation = false,
			dTranslation = false,
			dRotation = false
		});
		// level 3
		levelList.Add ( new Level {
			bTranslation = true,
			bRotation = false,
			dTranslation = true,
			dRotation = false
		});
		// level 4
		levelList.Add ( new Level {
			bTranslation = true,
			bRotation = true,
			dTranslation = true,
			dRotation = false
		});
		// level 5
		levelList.Add ( new Level {
			bTranslation = true,
			bRotation = true,
			dTranslation = true,
			dRotation = true
		});
	}

	void LevelYOffset (float yOffset) {
		gameObject.GetComponent<GenerateMap> ().LevelYOffset (yOffset);
	}

	void PowerLocalPosition (GameObject powerObj) {
		gameObject.GetComponent<GenerateMap> ().PowerLocalPosition (powerObj);
	}

	// generate map
	void GenerateMap (GameObject newGameObject) {
		Bounds newBound = newGameObject.transform.Find ("Body").GetComponent<Collider> ().bounds;
		gameObject.GetComponent<GenerateMap> ().GenerateMapFrombounds (newBound);
	}
}
