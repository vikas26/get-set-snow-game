﻿using UnityEngine;
using System.Collections;

public class EyeMovement : MonoBehaviour {

	GameObject player;
	float activeInDistance = 5f;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 eyePos = gameObject.transform.position;
		Vector3 playerPos = player.transform.position;
		playerPos.z = eyePos.z;

		if (Vector3.Distance (eyePos, playerPos) < activeInDistance) {
			Vector3 v = Vector3.MoveTowards (gameObject.transform.position, player.transform.position, Time.deltaTime * 0.5f);
			v.z = gameObject.transform.position.z;
			gameObject.transform.position = v;
		}
	}

}
