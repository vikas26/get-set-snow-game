﻿using UnityEngine;
using System.Collections;

public class FlipFumes : MonoBehaviour {

	private float afterTime = 0.2f;
	private float currenttime;

	// Use this for initialization
	void Start () {
		currenttime = Time.time + afterTime;
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > currenttime) {
			currenttime = Time.time + afterTime;
			if (GetComponent<SpriteRenderer> ().flipX) {
				GetComponent<SpriteRenderer> ().flipX = false;
			} else {
				GetComponent<SpriteRenderer> ().flipX = true;
			}
		}
	}
}
