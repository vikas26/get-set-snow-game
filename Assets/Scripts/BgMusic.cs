﻿using UnityEngine;
using System.Collections;

public class BgMusic : MonoBehaviour {

	public static BgMusic instance;

	void Awake() {
		if (instance) {
			DestroyImmediate (gameObject);
		} else {
			DontDestroyOnLoad (gameObject);
			instance = this;
		}
	}
}
