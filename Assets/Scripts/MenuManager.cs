﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI; 

public class MenuManager : MonoBehaviour {

	GameObject myCanvas;

	public Toggle musicToggle;
	public Toggle sfxToggle;
	public Dropdown manoeuvringOption;
	public Slider tiltSensitivityslider;

	// Use this for initialization
	void Start () {
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		Debug.Log (Application.version);
		myCanvas = GameObject.FindGameObjectWithTag("Canvas");

		InitializeSettingsOptions ();

		GoogleAnalyticsLog ("Menu");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (myCanvas.transform.FindChild ("SettingsPanel").transform.localPosition.x < 100) {
				HideSettings ();
			} else if (myCanvas.transform.FindChild ("UpcomingPanel").transform.localPosition.x < 100) {
				HideUpcoming ();
			} else {
				Application.Quit ();
			}
		}
	}

	// Play one player game
	public void PlayOnePlayerGame () {
		DontDestroyOnLoad (GameObject.FindGameObjectWithTag ("BgMusic"));
		SceneManager.LoadScene ("game");
	}

	// share score
	public void ShareGame () {
		GoogleAnalyticsLog ("Menu - Share Game");
		string subject = PlayerPrefs.GetString ("gameName");
		string body = "Hey, I am climbing up the leaderboard in this new cool game. Can you? " 
			+ "#" + PlayerPrefs.GetString ("gameName").Trim ().Replace (" ", "")
			+ " " + PlayerPrefs.GetString ("gameLink");
		gameObject.GetComponent<ShareScript> ().ShareText (subject, body);
	}

	// No ad
	public void NoAd () {
		GoogleAnalyticsLog ("Menu - No Ad");
		Debug.Log ("No Ad");
	}

	// Show Settings
	public void ShowSettings () {
		GoogleAnalyticsLog ("Menu - Settings");
		Animation settingsAnimation = myCanvas.transform.FindChild ("SettingsPanel").GetComponent<Animation> ();
		settingsAnimation ["SettingsSlideIn"].speed = 1;
		settingsAnimation ["SettingsSlideIn"].time = 0;
		settingsAnimation.Play ("SettingsSlideIn", PlayMode.StopAll);
		// myCanvas.transform.FindChild ("SettingsPanel").gameObject.SetActive (true);
	}

	// Hide settings
	public void HideSettings () {
		Animation settingsAnimation = myCanvas.transform.FindChild ("SettingsPanel").GetComponent<Animation> ();
		settingsAnimation ["SettingsSlideIn"].speed = -1;
		settingsAnimation ["SettingsSlideIn"].time = settingsAnimation ["SettingsSlideIn"].length;
		settingsAnimation.Play ("SettingsSlideIn", PlayMode.StopAll);
		// myCanvas.transform.FindChild ("SettingsPanel").gameObject.SetActive (false);
	}

	// Music settings
	public void MusicSettings () {
		if (musicToggle.isOn) {
			PlayerPrefs.SetString ("playMusicFlag", "true");
			GameObject.FindGameObjectWithTag ("BgMusic").GetComponent<AudioSource> ().enabled = true;
		} else {
			PlayerPrefs.SetString ("playMusicFlag", "false");
			GameObject.FindGameObjectWithTag ("BgMusic").GetComponent<AudioSource> ().enabled = false;
		}
	}

	// Special sound effects settings
	public void SFXSettings () {
		if (sfxToggle.isOn) {
			PlayerPrefs.SetString ("playSFXFlag", "true");
		} else {
			PlayerPrefs.SetString ("playSFXFlag", "false");
		}
	}

	// Manoeuvring way - touch/tilt
	public void ManoeuvringSettings () {
		// options values - 0 - tilt, 1 - touch
		// set - 1 - tilt, 2 - touch
		PlayerPrefs.SetInt ("manoeuvringWay", manoeuvringOption.value + 1);
		EnableDisableTiltSensitivity (manoeuvringOption.value);
	}

	// Tilt sensitivity settings
	public void TiltSensitivitySettings () {
		float sensitivity = TiltSensitivityCalculations (tiltSensitivityslider.value);
		PlayerPrefs.SetString ("tiltSensitivity", sensitivity.ToString ());
	}

	// Calculation of tilt sensitivity
	float TiltSensitivityCalculations (float x, bool valueToSettingsFlag = false) {
		// 0 - 0.6, 1 - 0.03
		// using line equation for two variables
		// (y - y1)/(y2 - y1) = (x - x1)/(x2 - x1)
		float y1 = 0.6f;
		float y2 = 0.03f;
		float x1 = 0f;
		float x2 = 1f;
		if (valueToSettingsFlag) {
			float temp1 = y1; y1 = x1; x1 = temp1;
			float temp2 = y2; y2 = x2; x2 = temp2;
		}
		return ((y2 - y1) * (x - x1) / (x2 - x1)) + y1;
	}

	// initailize settings from vars
	void InitializeSettingsOptions () {
		musicToggle.isOn = ParseBool (PlayerPrefs.GetString ("playMusicFlag"));
		sfxToggle.isOn = ParseBool (PlayerPrefs.GetString ("playSFXFlag"));
		manoeuvringOption.value = PlayerPrefs.GetInt ("manoeuvringWay") - 1;
		EnableDisableTiltSensitivity (manoeuvringOption.value);
		Debug.Log (PlayerPrefs.GetString ("tiltSensitivity"));
		tiltSensitivityslider.value = TiltSensitivityCalculations (float.Parse (PlayerPrefs.GetString ("tiltSensitivity")), true);

		GoogleAnalyticsLog ("Menu - Background Music - " + PlayerPrefs.GetString ("playMusicFlag"));
		GoogleAnalyticsLog ("Menu - SFX - " + PlayerPrefs.GetString ("playSFXFlag"));
		GoogleAnalyticsLog ("Menu - Manoeuvring - " + GetManoeuvringWay ());
	}

	// enable disable interactability of tilt sensitivity slider
	void EnableDisableTiltSensitivity (int value) {
		if (value == 0) {
			tiltSensitivityslider.interactable = true;
		} else {
			tiltSensitivityslider.interactable = false;
		}
	}

	// returns true or false based on strings
	bool ParseBool (string value) {
		if (value == "true") {
			return true;
		} else {
			return false;
		}
	}

	// re intialize all player prefs vars
	public void ReInitializeVars () {
		GetComponent<GlobalVariables> ().ReInitializeVars ();
		InitializeSettingsOptions ();
		GoogleAnalyticsLog ("Menu - Settings - Reset");
	}

	// get manoeuvring way
	string GetManoeuvringWay () {
		// options values - 0 - tilt, 1 - touch
		// set - 1 - tilt, 2 - touch
		string[] manoWays = {"Tilt", "Touch"};
		return manoWays[PlayerPrefs.GetInt ("manoeuvringWay") - 1];
	}

	// Show Upcoming
	public void ShowUpcoming () {
		GoogleAnalyticsLog ("Menu - Upcoming");
		Animation upcomingAnimation = myCanvas.transform.FindChild ("UpcomingPanel").GetComponent<Animation> ();
		upcomingAnimation ["SettingsSlideIn"].speed = 1;
		upcomingAnimation ["SettingsSlideIn"].time = 0;
		upcomingAnimation.Play ("SettingsSlideIn", PlayMode.StopAll);
	}

	// Hide settings
	public void HideUpcoming () {
		Animation upcomingAnimation = myCanvas.transform.FindChild ("UpcomingPanel").GetComponent<Animation> ();
		upcomingAnimation ["SettingsSlideIn"].speed = -1;
		upcomingAnimation ["SettingsSlideIn"].time = upcomingAnimation ["SettingsSlideIn"].length;
		upcomingAnimation.Play ("SettingsSlideIn", PlayMode.StopAll);
	}

	// rate us now
	public void RateUsNow () {
		GoogleAnalyticsLog ("Menu - Rate Game");
		Application.OpenURL(PlayerPrefs.GetString ("gameLink"));
		PlayerPrefs.SetString ("ratedGame", "true");
	}

	// log google analytics
	public void GoogleAnalyticsLog (string content) {
		if (GoogleAnalytics.instance) {
			GoogleAnalytics.instance.LogScreen (content);
		}
	}
}
