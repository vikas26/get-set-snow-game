﻿using UnityEngine;
using System.Collections;

public class EyeMovementTowardsEnemy : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		GameObject neartestEnemy = GetNearestEnemy ();
		if (neartestEnemy != null) {
			Vector3 v = Vector3.MoveTowards (gameObject.transform.position, neartestEnemy.transform.position, Time.deltaTime * 0.5f);
			v.z = gameObject.transform.position.z;
			gameObject.transform.position = v;
		}
	}

	// Nearest Enemy
	GameObject GetNearestEnemy () {
		GameObject tMin = null;
		float minDistance = 5f;
		Vector3 currentPos = gameObject.transform.position;

		GameObject[] destroyers = GameObject.FindGameObjectsWithTag ("DestroyerPupil");
		foreach (GameObject t in destroyers) {
			currentPos.z = t.transform.position.z;
			float dist = Vector3.Distance(t.transform.position, currentPos);
			if (dist < minDistance) {
				tMin = t;
				minDistance = dist;
			}
		}

		GameObject[] blockers = GameObject.FindGameObjectsWithTag ("BlockerPupil");
		foreach (GameObject t in blockers) {
			currentPos.z = t.transform.position.z;
			float dist = Vector3.Distance(t.transform.position, currentPos);
			if (dist < minDistance) {
				tMin = t;
				minDistance = dist;
			}
		}
		return tMin;
	}
}
