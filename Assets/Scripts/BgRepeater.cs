﻿using UnityEngine;
using System.Collections;

public class BgRepeater : MonoBehaviour {

	public float yBackgroundShiftBy;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// On Collision
	void OnTriggerEnter (Collider colliderObject) {
		// on collision with background, take bg down
		if (colliderObject.tag == "Background") {
			Vector3 bgPosition = colliderObject.gameObject.transform.position;
			bgPosition.y += yBackgroundShiftBy;
			colliderObject.gameObject.transform.position = bgPosition;
		}
	}
}
