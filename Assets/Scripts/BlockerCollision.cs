﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlockerCollision : MonoBehaviour {

	List<GameObject> powers = new List<GameObject>();
	List<GameObject> scoreColliders = new List<GameObject>();

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	// On Collision Enter
	void OnTriggerEnter (Collider colliderObject) {
		switch (colliderObject.tag) {
		case "BlockerBody":
			Camera.main.GetComponent<Manager> ().BlockerCollisionSound ();
			break;
		case "DestroyerBody":
			Camera.main.GetComponent<Manager> ().GameOver ();
			break;
		case "LevelChanger":
			Camera.main.GetComponent<Manager> ().NextLevel (colliderObject.GetComponent<LevelChanger> ().ChangeLevelTo, colliderObject.gameObject);
			break;
		case "SmallBallPower":
			Camera.main.GetComponent<Manager> ().BallPower (2, colliderObject.GetComponent<PowerColliderVars>().powerTime, true);
			powers.Add (colliderObject.gameObject);
			colliderObject.gameObject.SetActive (false);
			break;
		case "InvisibleBallPower":
			Camera.main.GetComponent<Manager> ().BallPower (3, colliderObject.GetComponent<PowerColliderVars>().powerTime, true);
			powers.Add (colliderObject.gameObject);
			colliderObject.gameObject.SetActive (false);
			break;
		case "AIPower":
			Camera.main.GetComponent<Manager> ().BallPowerObject (colliderObject.gameObject);
			Camera.main.GetComponent<Manager> ().BallPower (4, colliderObject.GetComponent<PowerColliderVars> ().powerTime, true);
			Camera.main.GetComponent<Manager> ().AIBallPowerPath (colliderObject.GetComponent<PowerColliderVars> ().pathOfAI);
			powers.Add (colliderObject.gameObject);
			colliderObject.gameObject.SetActive (false);
			break;
		case "ComingSoonLevels":
			Camera.main.GetComponent<Manager> ().ShowComingSoonPanel ();
			break;
		case "BlockerScore":
		case "DestroyerScore":
			if (!colliderObject.GetComponent<AddScore> ().scoreAdded) {
				Camera.main.GetComponent<Manager> ().IncreaseScore (colliderObject.GetComponent<AddScore> ().addScore);
				colliderObject.GetComponent<AddScore> ().scoreAdded = true;
				scoreColliders.Add (colliderObject.gameObject);
			}
			break;
		}
	}

	// On Collision Exit
	void OnTriggerExit (Collider colliderObject) {
		switch (colliderObject.tag) {
		case "DestroyerBody":
			PlayerPrefs.SetString ("stopPlayerVerticalMovementFlag", "false");
			break;
		}
	}

	// Make powers visible
	public void ShowPowers () {
		foreach (GameObject power in powers) {
			power.SetActive (true);
		}
		powers.Clear ();
	}

	// Show score increments
	public void ShowScoreColliders () {
		foreach (GameObject scoreCollider in scoreColliders) {
			scoreCollider.GetComponent<AddScore> ().scoreAdded = false;
		}
		scoreColliders.Clear ();
	}
}
