﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalVariables : MonoBehaviour {

	private List<Color32> levelColors = new List<Color32>();

	// Use this for initialization
	void Awake () {
		InitializeVars ();
	}

	// initialize vars
	void InitializeVars () {
		// game name
		PlayerPrefs.SetString ("gameName", "Get Set Snow");
		PlayerPrefs.SetString ("gameLink", "https://play.google.com/store/apps/details?id=com.vikas.getsetsnow");

		// movement vars
		PlayerPrefs.SetString ("stopPlayerVerticalMovementFlag", "false");
		PlayerPrefs.SetString ("stopPlayerLeftMovementFlag", "false");
		PlayerPrefs.SetString ("stopPlayerRightMovementFlag", "false");

		// level and score vars
		PlayerPrefs.SetInt ("levelNumber", 0);
		PlayerPrefs.SetInt ("currentScore", 0);

		// highest score vars
		PlayerPrefs.SetString ("scoredHighestScoreFlag", "false");
		if (!PlayerPrefs.HasKey ("highestScore")) {
			PlayerPrefs.SetInt ("highestScore", 0);
		}

		// game paused flag
		PlayerPrefs.SetString ("gamePaused", "false");

		// game over flag
		PlayerPrefs.SetString ("gameOver", "false");

		// player initial vars for restarting game
		PlayerPrefs.SetString ("playerInitialPosition", "0;3;0");
		PlayerPrefs.SetString ("cameraInitialPosition", "0;0;-10");

		// current ball
		// 1 - normal, 2 - small, 3 - invisible, 4 - stone, 5 - fluid
		PlayerPrefs.SetInt ("ballType", 1);

		// power countdown
		PlayerPrefs.SetString ("powerCountdown", "false");
		PlayerPrefs.SetString ("pausePowerCountdown", "false");

		// drag values
		if (!PlayerPrefs.HasKey ("playerInitialDrag")) {
			PlayerPrefs.SetFloat ("playerInitialDrag", 0f);
		}
		PlayerPrefs.SetFloat ("playerPauseDrag", 10000f);

		// settings - music, sfx
		if (!PlayerPrefs.HasKey ("playMusicFlag")) {
			PlayerPrefs.SetString ("playMusicFlag", "true");
			PlayerPrefs.SetString ("playSFXFlag", "true");
		}

		// settings - player manoeuvring elocity = movement * speed; 
		if (!PlayerPrefs.HasKey ("manoeuvringWay")) {
			// 1 - Tilt, 2 - Touch
			PlayerPrefs.SetInt ("manoeuvringWay", 2);
			PlayerPrefs.SetString ("tiltSensitivity", "0.1");
		}

		// Game played vars
		if (!PlayerPrefs.HasKey ("gamePlayedByTouchTimes")) {
			PlayerPrefs.SetInt ("gamePlayedByTouchTimes", 0);
			PlayerPrefs.SetString ("touchGameInstructions", "false");
			PlayerPrefs.SetString ("powerInstructions", "false");
			PlayerPrefs.SetInt ("gamePlayedByTiltTimes", 0);
		}

		// coming soon panel
		PlayerPrefs.SetString ("comingSoonPanel", "false");

		// google play games logged in
		PlayerPrefs.SetString ("playGamesLoggedIn", "false");

		// rated
		if (!PlayerPrefs.HasKey ("ratedGameOverTimes")) {
			PlayerPrefs.SetInt ("ratedGameOverTimes", 0);
			PlayerPrefs.SetString ("ratedGame", "false");
		}

		// set colors for each level
		SetLevelColors ();

		// set bg music on/off
		SetBgMusic ();
	}

	// these are reset when reset button is pressed
	void ResetVars () {
		PlayerPrefs.DeleteKey ("playMusicFlag");
		PlayerPrefs.DeleteKey ("playSFXFlag");
		PlayerPrefs.DeleteKey ("manoeuvringWay");
		PlayerPrefs.DeleteKey ("tiltSensitivity");
		PlayerPrefs.DeleteKey ("gamePlayedByTouchTimes");
		PlayerPrefs.DeleteKey ("touchGameInstructions");
		PlayerPrefs.DeleteKey ("powerInstructions");
		PlayerPrefs.DeleteKey ("gamePlayedByTiltTimes");
	}

	void SetLevelColors () {
		levelColors.Clear ();
		levelColors.Add (new Color32 (20, 111, 99, 255));	// dark green 
		levelColors.Add (new Color32 (239, 102, 102, 255));	// light red
		levelColors.Add (new Color32 (0, 87, 160, 255));	// dark blue
		levelColors.Add (new Color32 (78, 164, 207, 255));	// light blue
		levelColors.Add (new Color32 (231, 110, 0, 255));	// dark orange
		levelColors.Add (new Color32 (94, 44, 11, 255));	// dark brown
		levelColors.Add (new Color32 (126, 52, 157, 255));	// light voilet
		levelColors.Add (new Color32 (103, 90, 90, 255));	// dark grey
		levelColors.Add (new Color32 (188, 161, 107, 255));	// light pink

		PlayerPrefs.SetInt ("levelColorsCount", levelColors.Count);

		for (int i = 0; i < levelColors.Count; i++) {
			string rgba = levelColors [i].r.ToString ()
			              + "," + levelColors [i].g.ToString ()
			              + "," + levelColors [i].b.ToString ()
			              + "," + levelColors [i].a.ToString ();
			PlayerPrefs.SetString ("levelColors"+(i+1).ToString(), rgba);
		}
	}

	// Set bg music on/off
	void SetBgMusic () {
		if (GameObject.FindGameObjectWithTag ("BgMusic")) {
			if (PlayerPrefs.GetString ("playMusicFlag") == "true") {
				GameObject.FindGameObjectWithTag ("BgMusic").SetActive (true);
			} else {
				GameObject.FindGameObjectWithTag ("BgMusic").SetActive (false);
			}
		}
	}

	// re intialize vars after deleting them
	public void ReInitializeVars () {
		ResetVars ();
		InitializeVars ();
	}
}
