﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour {

	GameObject myCanvas;
	GameObject player;

	IDictionary<int, string> ballTags = new Dictionary<int, string>();

	public AudioClip powerSoundClip;
	public AudioClip restrictorCollisionSoundClip;
	public AudioClip gameOverSoundClip;

	private AudioSource audioSource;

	UnityAdsButton adManager;
	int showAdAfterGameOver = 10;

	private PlayGameServices playGameServices;

	// follow ai path
	private List<Vector2> aiBallPath = new List<Vector2> ();
	private bool followAIPathFlag = false;
	private int followAIPathIndex = 1;
	private float aiBallSpeed = 0f;
	private int aiPassRestrictors;
	private int aiRestrictorsLeft;

	// Use this for initialization
	void Start () {
		myCanvas = GameObject.FindGameObjectWithTag("Canvas");

		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		audioSource = GetComponent<AudioSource> ();
		player = GameObject.FindGameObjectWithTag("Player");
		PlayerPrefs.SetFloat ("playerInitialDrag", player.GetComponent<Rigidbody> ().drag);
		SetBallTags ();

		adManager = GameObject.FindGameObjectWithTag ("AdManager").GetComponent<UnityAdsButton> ();
		playGameServices = GameObject.Find ("PlayGamesManager").GetComponent<PlayGameServices> ();

		// Show instructions
		// playing game for the first time
		if (PlayerPrefs.GetInt ("gamePlayedByTouchTimes") == 0 && PlayerPrefs.GetInt ("gamePlayedByTiltTimes") == 0) {
			ShowPowerInstructions ();
		} else {
			PauseGame ();
			ResumeGame ();
		}
		switch (PlayerPrefs.GetInt ("manoeuvringWay")) {
		case 1:
			// tilt
			PlayerPrefs.SetInt ("gamePlayedByTiltTimes", PlayerPrefs.GetInt ("gamePlayedByTiltTimes") + 1);
			break;
		case 2:
			//touch
			PlayerPrefs.SetInt ("gamePlayedByTouchTimes", PlayerPrefs.GetInt ("gamePlayedByTouchTimes") + 1);
			break;
		}

		// google play games auto sign in
		GooglePlayServicesSignIn ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (PlayerPrefs.GetString ("gameOver") == "true") {
				GoToMenu ();
			} else if (PlayerPrefs.GetString ("powerInstructions") == "true") {
				ShowGameInstructions ();
			} else if (PlayerPrefs.GetString ("touchGameInstructions") == "true") {
				HideGameInstructions ();
			} else if (PlayerPrefs.GetString ("comingSoonPanel") == "true") {
				RestartGame ();
			} else if (PlayerPrefs.GetString ("gamePaused") == "true") {
				ResumeGame ();
			} else {
				PauseGame ();
			}
		}

		if (followAIPathFlag) {
			FollowAIPath ();
		}
	}

	// Game over
	public void GameOver () {
		GoogleAnalyticsLog ("Game - Game Over - Level " + PlayerPrefs.GetInt ("levelNumber").ToString () + " - Score " + PlayerPrefs.GetInt ("currentScore"));

		// posting highest score to leader board of game services
		if (PlayerPrefs.GetString ("playGamesLoggedIn") == "true") {
			playGameServices.PostScoreToLeaderboard (PlayerPrefs.GetInt ("currentScore"));
		}

		// show ad on game over
		bool showAdFlag = true;
		if (PlayerPrefs.GetInt ("gameOverTimes") < showAdAfterGameOver) {
			PlayerPrefs.SetInt ("gameOverTimes", PlayerPrefs.GetInt ("gameOverTimes") + 1);
			showAdFlag = false;
		}
		if (adManager.AdReady () && showAdFlag) {
			adManager.ShowAd ();
			PlayerPrefs.SetInt ("gameOverTimes", 0);
		}

		// rate us
		PlayerPrefs.SetInt ("ratedGameOverTimes", PlayerPrefs.GetInt ("ratedGameOverTimes") + 1);
		if (PlayerPrefs.GetString ("ratedGame") == "false" && (PlayerPrefs.GetInt ("ratedGameOverTimes") % 3 == 0)) {
			OpenRateUsPopup ();
		}

		PlayerPrefs.SetString ("gameOver", "true");
		PlayerPrefs.SetString ("gamePaused", "true");
		gameObject.GetComponent<GUIRenderer> ().GameOver ();

		if (PlayerPrefs.GetString ("playSFXFlag") == "true") {
			audioSource.PlayOneShot (gameOverSoundClip);
		}

		if (PlayerPrefs.GetString ("powerCountdown") == "true") {
			gameObject.GetComponent<GUIRenderer> ().StopPowerCountdown ();
		}
		player.GetComponent<Rigidbody> ().drag = PlayerPrefs.GetFloat ("playerPauseDrag");
	}

	// pause game
	public void PauseGame () {
		PlayerPrefs.SetString ("gamePaused", "true");
		gameObject.GetComponent<GUIRenderer> ().PauseGame ();
		player.GetComponent<Rigidbody> ().drag = PlayerPrefs.GetFloat ("playerPauseDrag");
	}

	// resume game
	public void ResumeGame () {
		gameObject.GetComponent<GUIRenderer> ().ResumeGame ();
	}

	// restart game
	public void RestartGame () {
		player.GetComponent<Rigidbody> ().drag = PlayerPrefs.GetFloat ("playerInitialDrag");
		player.GetComponent<Rigidbody> ().useGravity = false;
		player.GetComponent<Rigidbody> ().useGravity = true;

		gameObject.GetComponent<GUIRenderer> ().RestartGame ();

		BallPower (1, 0, false);
		PlayerPrefs.SetString ("gameOver", "false");
		player.GetComponent<BlockerCollision> ().ShowPowers ();
		player.GetComponent<BlockerCollision> ().ShowScoreColliders ();
		ResetCurrentScore ();
		ResetLevel ();
		SetBgColor (1);
	}

	// go to menu
	public void GoToMenu () {
		DestroyImmediate (GameObject.FindGameObjectWithTag ("BgMusic")); 
		SceneManager.LoadScene ("menu");
	}

	// Next level
	public void NextLevel (int changeLevelTo, GameObject colliderObject = null) {
		GoogleAnalyticsLog ("Game - Next Level - Level " + changeLevelTo.ToString ());
		PlayerPrefs.SetInt ("levelNumber", changeLevelTo);

		if (colliderObject != null && colliderObject.GetComponent<LevelChanger>().GenerateNextLevel) {
			gameObject.GetComponent<GenerateMap> ().PlayerBound (player.GetComponentInChildren<Collider> ().bounds);
			gameObject.GetComponent<PathCreator> ().CreateWholeLevel (changeLevelTo, colliderObject);
		}

		gameObject.GetComponent<GUIRenderer> ().ShowLevel (changeLevelTo);
		SetBgColor (changeLevelTo);

		DisableMoveItself ();
		EnableMoveItself (changeLevelTo);
	}

	// Increase score and set highest score
	public void IncreaseScore (int step) {
		if (PlayerPrefs.GetInt ("ballType") == 4) {
			aiRestrictorsLeft -= 1;
			gameObject.GetComponent<GUIRenderer> ().ShowAIDisplay (aiRestrictorsLeft, true);
		}
		int newScore = PlayerPrefs.GetInt ("currentScore") + step;
		PlayerPrefs.SetInt ("currentScore", newScore);
		if (newScore > PlayerPrefs.GetInt ("highestScore")) {
			PlayerPrefs.SetInt ("highestScore", newScore);
			PlayerPrefs.SetString ("scoredHighestScoreFlag", "true");
		}
	}

	// Reset current score
	void ResetCurrentScore () {
		PlayerPrefs.SetInt ("currentScore", 0);
	}

	// Reset highest score
	void ResetHighestScore () {
		PlayerPrefs.SetInt ("highestScore", 0);
	}

	// Reset Level
	void ResetLevel () {
		PlayerPrefs.SetInt ("levelNumber", 0);
	}

	// get level color
	Color32 GetLevelColor (int levelNumber) {
		string levelColorStr = PlayerPrefs.GetString ("levelColors" + (((levelNumber - 1) % PlayerPrefs.GetInt ("levelColorsCount")) + 1).ToString ());
		byte colorR = byte.Parse (levelColorStr.Split ("," [0]) [0]);
		byte colorG = byte.Parse (levelColorStr.Split ("," [0]) [1]);
		byte colorB = byte.Parse (levelColorStr.Split ("," [0]) [2]);
		byte colorA = byte.Parse (levelColorStr.Split ("," [0]) [3]);
		return new Color32 (colorR, colorG, colorB, colorA);
	}

	// set bg color according to level
	void SetBgColor (int levelNumber, float transitionTime = 2f) {
		Color32 nextLevelColor = GetLevelColor (levelNumber);
		gameObject.GetComponent<BackgroundColorTransition> ().LevelChange (nextLevelColor, transitionTime);
	}

	// share score
	public void ShareScoreText () {
		GoogleAnalyticsLog ("Game - Share Game - Score " + PlayerPrefs.GetInt ("currentScore").ToString ());
		string subject = "";
		string body = "Hey, I just scored "
		              + PlayerPrefs.GetInt ("currentScore").ToString () + " in #"
		              + PlayerPrefs.GetString ("gameName").Trim ().Replace (" ", "")
		              + ". Can you score better? " + PlayerPrefs.GetString ("gameLink");
		gameObject.GetComponent<ShareScript> ().ShareText (subject, body);
	}

	// Set ball tags
	void SetBallTags () {
		ballTags [1] = "NormalBall";
		ballTags [2] = "SmallBall";
		ballTags [3] = "InvisibleBall";
		ballTags [4] = "AIBall";
		ballTags [5] = "FluidBall";
	}

	// 1 - normal, 2 - small, 3 - invisible, 4 - AI, 5 - fluid
	public void BallPower (int ballType, int powerCountdownTime, bool showFlag = true, bool powerHidden = false) {
		if (ballType > 1 && PlayerPrefs.GetString ("playSFXFlag") == "true") {
			audioSource.PlayOneShot (powerSoundClip);
			GoogleAnalyticsLog ("Game - Level " + PlayerPrefs.GetInt ("levelNumber") + " - Power " + ballTags [ballType].ToString ());
		}

		if (ballType == 1 && powerHidden) {
			AllowMovementMoveItself (PlayerPrefs.GetInt ("levelNumber"));
		}
		if (ballType == 4) {
			// AI power
			aiRestrictorsLeft = aiPassRestrictors;
			followAIPathIndex = 1;
			followAIPathFlag = true;
			InitialStateMoveItself (PlayerPrefs.GetInt ("levelNumber"));
			showFlag = false;
			gameObject.GetComponent<GUIRenderer> ().ShowAIDisplay (aiRestrictorsLeft);
		}

		for (int i = 0; i < player.transform.childCount; i++) {
			if (ballTags [ballType].Trim().ToLower() == player.transform.GetChild (i).tag.Trim().ToLower()) {
				player.transform.GetChild (i).gameObject.SetActive (true);
			} else {
				player.transform.GetChild (i).gameObject.SetActive (false);
			}
		}
		PlayerPrefs.SetInt ("ballType", ballType);
		if (showFlag) {
			gameObject.GetComponent<GUIRenderer> ().PowerCountdown (powerCountdownTime, 1);
		}
	}

	// disable moveitself script from all restrictors
	public void DisableMoveItself () {
		GameObject blockersParent = GameObject.Find ("Restrictors/Blockers");
		GameObject destroyersParent = GameObject.Find ("Restrictors/Destroyers");
		foreach (Transform bLevel in blockersParent.transform) {
			// blocker levels
			MoveItselfScriptToggle (bLevel, false);
		}
		foreach (Transform dLevel in destroyersParent.transform) {
			// destroyer levels
			MoveItselfScriptToggle (dLevel, false);
		}
	}

	// initial state of all enemies
	public void InitialStateMoveItself (int levelNumber) {
		GameObject blockersParent = GameObject.Find ("Restrictors/Blockers");
		Transform bLevel = blockersParent.transform.GetChild (levelNumber - 1).transform;
		InitialStateMoveItselfScript (bLevel, true);

		GameObject destroyersParent = GameObject.Find ("Restrictors/Destroyers");
		Transform dLevel = destroyersParent.transform.GetChild (levelNumber - 1).transform;
		InitialStateMoveItselfScript (dLevel, true);
	}

	// initial state of all enemies
	public void AllowMovementMoveItself (int levelNumber) {
		GameObject blockersParent = GameObject.Find ("Restrictors/Blockers");
		Transform bLevel = blockersParent.transform.GetChild (levelNumber - 1).transform;
		InitialStateMoveItselfScript (bLevel, false);

		GameObject destroyersParent = GameObject.Find ("Restrictors/Destroyers");
		Transform dLevel = destroyersParent.transform.GetChild (levelNumber - 1).transform;
		InitialStateMoveItselfScript (dLevel, false);
	}

	// enable moveitself script of all restrictors for that level
	void InitialStateMoveItselfScript (Transform level, bool flag) {
		foreach (Transform child in level) {
			if (child.GetComponent<MoveItself> ()) {
				if (flag) {
					child.GetComponent<MoveItself> ().TakeToInitialState ();
				} else {
					child.GetComponent<MoveItself> ().AllowMovement ();
				}
			}
		}
	}

	// enable moveitself script from all restrictors for that level
	public void EnableMoveItself (int levelNumber) {
		GameObject blockersParent = GameObject.Find ("Restrictors/Blockers");
		Transform bLevel = blockersParent.transform.GetChild (levelNumber - 1).transform;
		MoveItselfScriptToggle (bLevel, true);

		GameObject destroyersParent = GameObject.Find ("Restrictors/Destroyers");
		Transform dLevel = destroyersParent.transform.GetChild (levelNumber - 1).transform;
		MoveItselfScriptToggle (dLevel, true);
	}

	// enable moveitself script of all restrictors for that level
	void MoveItselfScriptToggle (Transform level, bool flag) {
		foreach (Transform child in level) {
			if (child.GetComponent<MoveItself> ()) {
				child.GetComponent<MoveItself> ().enabled = flag;
			}
			if (child.FindChild ("Eyes/Pupil").GetComponent<EyeMovement>()) {
				child.FindChild ("Eyes/Pupil").GetComponent<EyeMovement> ().enabled = flag;
			}
			if (level.parent.name == "Destroyers") {
				if (child.FindChild ("Body/2d/Fumes").GetComponent<FlipFumes> ()) {
					child.FindChild ("Body/2d/Fumes").GetComponent<FlipFumes> ().enabled = flag;
				}
			}
		}
	}

	// blocker collision sound
	public void BlockerCollisionSound () {
		if (PlayerPrefs.GetString ("playSFXFlag") == "true") {
			audioSource.PlayOneShot (restrictorCollisionSoundClip);
		}
	}

	// Show power instructions
	public void ShowPowerInstructions () {
		PlayerPrefs.SetString ("gamePaused", "true");
		gameObject.GetComponent<GUIRenderer> ().ShowPowerInstructions ();
		player.GetComponent<Rigidbody> ().drag = PlayerPrefs.GetFloat ("playerPauseDrag");
	}

	// Show game instructions
	public void ShowGameInstructions () {
		gameObject.GetComponent<GUIRenderer> ().HidePowerInstructions ();
		gameObject.GetComponent<GUIRenderer> ().ShowGameInstructions ();
	}

	// Hide Instructions
	public void HideGameInstructions () {
		gameObject.GetComponent<GUIRenderer> ().HideGameInstructions ();
		player.GetComponent<Rigidbody> ().drag = PlayerPrefs.GetFloat ("playerInitialDrag");
		PlayerPrefs.SetString ("gamePaused", "false");
		PauseGame ();
		ResumeGame ();
	}

	// show coming soon panel
	public void ShowComingSoonPanel () {
		GoogleAnalyticsLog ("Game - Coming Soon - Level " + PlayerPrefs.GetInt ("levelNumber").ToString () + " - Score " + PlayerPrefs.GetInt ("currentScore"));
		PlayerPrefs.SetString ("gameOver", "true");
		PlayerPrefs.SetString ("gamePaused", "true");
		gameObject.GetComponent<GUIRenderer> ().ShowComingSoon ();
	}

	private void GooglePlayServicesSignIn () {
		// Try silent sign-in to google play services
		playGameServices.SignIn (false);

		if (SceneManager.GetActiveScene ().name == "game") {
			myCanvas.transform.FindChild ("Leaderboard").GetComponent<Button> ().onClick.RemoveAllListeners ();
			myCanvas.transform.FindChild ("Leaderboard").GetComponent<Button> ().onClick.AddListener (delegate() {
				playGameServices.ShowLeaderBoard ();
			});

			myCanvas.transform.FindChild ("GooglePlayGames").GetComponent<Button> ().onClick.RemoveAllListeners ();
			myCanvas.transform.FindChild ("GooglePlayGames").GetComponent<Button> ().onClick.AddListener (delegate() {
				playGameServices.SignIn (false);
			});
		}
	}

	// open rate us popup
	void OpenRateUsPopup () {
		gameObject.GetComponent<GUIRenderer> ().ShowRateUs ();
	}

	// rate us now
	public void RateUsNow () {
		GoogleAnalyticsLog ("Game - Rate Game");
		Application.OpenURL(PlayerPrefs.GetString ("gameLink"));
		PlayerPrefs.SetString ("ratedGame", "true");
		gameObject.GetComponent<GUIRenderer> ().HideRateUs ();
	}

	// rate us later
	public void RateUsLater () {
		gameObject.GetComponent<GUIRenderer> ().HideRateUs ();
	}

	// follow ai path
	void FollowAIPath () {
		if (aiRestrictorsLeft == 0) {
			followAIPathFlag = false;
			gameObject.GetComponent<GUIRenderer> ().HideAIDisplay ();
			return;
		}
		Vector3 oldAIPath = player.transform.position;
		Vector3 newAIPath = player.transform.position;
		float yDelay = 0f;
		for (int i = followAIPathIndex; i < aiBallPath.Count; i++) {
			if (aiBallPath [followAIPathIndex - 1].y > (player.transform.position.y + yDelay) && aiBallPath [i].y < (player.transform.position.y + yDelay)) {
				followAIPathIndex = i;
				newAIPath.x = aiBallPath [followAIPathIndex].x;
				break;
			}
		}

		player.transform.position = Vector3.Lerp (oldAIPath, newAIPath, Time.deltaTime * 4);
	}

	public void BallPowerObject (GameObject powerObject) {
		aiPassRestrictors = powerObject.GetComponent<PowerColliderVars> ().powerTime;
	}

	public void AIBallPowerPath (List<Vector2> tempAIBallPath) {
		aiBallPath = tempAIBallPath;
	}

	// log google analytics
	public void GoogleAnalyticsLog (string content) {
		if (GoogleAnalytics.instance) {
			GoogleAnalytics.instance.LogScreen (content);
		}
	}
}
