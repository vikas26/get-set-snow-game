﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathFinder : MonoBehaviour {

	private List<Vector2> lastCoordinates = new List<Vector2> ();
	private int counterVar;
	private int[,] grid2d;
	private Vector2 startingPoint;
	private Vector2 endingPoint;
	private List<Vector2> finalPath = new List<Vector2> ();

	// find path
	public void FindPath (int[,] tempGrid2d, Vector2 tempStartingPoint, Vector2 tempEndingPoint) {
		counterVar = 1;
		finalPath.Clear ();
		lastCoordinates.Clear ();

		grid2d = tempGrid2d;
		startingPoint = tempStartingPoint;
		endingPoint = tempEndingPoint;

		StartCoroutine (FindPathIenumerator ());
	}

	IEnumerator FindPathIenumerator () {
		// ending point 
		lastCoordinates.Clear ();
		UpdateGrid ((int)endingPoint.x, (int)endingPoint.y);

		// filling path iteratively
		while (lastCoordinates.Count != 0) {
			Vector2[] lastTimeCoordinates = new Vector2[lastCoordinates.Count];
			lastCoordinates.CopyTo (lastTimeCoordinates);
			lastCoordinates.Clear ();
			counterVar += 1;
			yield return new WaitForSeconds (0.01f);

			foreach (Vector2 lastCoordinate in lastTimeCoordinates) {
				FillEmptyNeighbors ((int)lastCoordinate.x, (int)lastCoordinate.y);
			}
		}
		yield return new WaitForSeconds (0.01f);

		// get path
		StartCoroutine (GetPath ());
	}

	void FillEmptyNeighbors (int row, int column) {
		if ((row - 1) >= 0 && grid2d [row - 1, column] == 0) {
			UpdateGrid (row - 1, column);
		}
		if ((row + 1) < grid2d.GetLength (0) && grid2d [row + 1, column] == 0) {
			UpdateGrid (row + 1, column);
		}
		if ((column - 1) >= 0 && grid2d [row, column - 1] == 0) {
			UpdateGrid (row, column - 1);
		}
		if ((column + 1) < grid2d.GetLength (1) && grid2d [row, column + 1] == 0) {
			UpdateGrid (row, column + 1);
		}
	}

	void UpdateGrid (int x, int y) {
		lastCoordinates.Add (new Vector2 (x, y));
		grid2d [x, y] = counterVar;
	}

	IEnumerator GetPath () {
		yield return new WaitForSeconds (0.01f);
		int x = (int)startingPoint.x;
		int y = (int)startingPoint.y;
		int initialCounterVar = grid2d [x, y];
		finalPath.Add (new Vector2 (x, y));

		Vector2 destinationCoordinates = new Vector2 ((int)endingPoint.x, (int)endingPoint.y);
		while (Vector2.Distance (destinationCoordinates, startingPoint) != 0) {
			yield return new WaitForSeconds (0.01f);
			destinationCoordinates = GetNearestCounterVar (x, y, initialCounterVar);
			x = (int)destinationCoordinates.x;
			y = (int)destinationCoordinates.y;
			initialCounterVar -= 1;
			finalPath.Add (destinationCoordinates);
			if (initialCounterVar == 1) {
				break;
			}
		}
		gameObject.GetComponent<GenerateMap> ().FinalPath (finalPath);
	}

	Vector2 GetNearestCounterVar (int row, int column, int initialCounterVar) {
		Vector2 result = new Vector2 ();
		if ((row - 1) >= 0 && grid2d [row - 1, column] == initialCounterVar - 1) {
			result.x = row - 1;
			result.y = column;
		} else if ((row + 1) < grid2d.GetLength (0) && grid2d [row + 1, column] == initialCounterVar - 1) {
			result.x = row + 1;
			result.y = column;
		} else if ((column - 1) >= 0 && grid2d [row, column - 1] == initialCounterVar - 1) {
			result.x = row;
			result.y = column - 1;
		} else if ((column + 1) < grid2d.GetLength (1) && grid2d [row, column + 1] == initialCounterVar - 1) {
			result.x = row;
			result.y = column + 1;
		}
		return result;
	}

	void GridState (int[,] grid2d) {
		for (int i = 0; i < grid2d.GetLength(1); i++) {
			string row = "";
			for (int j = 0; j < grid2d.GetLength(0); j++) {
				row += grid2d[j, i] + " ";
			}
			Debug.Log (row);
		}
	}
}
