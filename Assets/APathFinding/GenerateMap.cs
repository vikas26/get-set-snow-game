﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GenerateMap : MonoBehaviour {

	// powerObject 
	private GameObject powerGameObject;

	// grid
	private int tileSize = 2;
	private int width = 6;
	private int height = 15;
	public int[,] grid2d;

	// player
	private float playerSize;
	private float xMin = -2.7f;
	private float xMax = 2.7f;
	private float yStart;
	private float levelYOffset;
		
	// starting and ending points
	private Vector2 startingPoint;
	private Vector2 endingPoint;

	void Start () {
		grid2d = new int[width * tileSize, height * tileSize];
	}

	public void PlayerBound (Bounds playerBound) {
		playerSize = 2 * playerBound.extents.x;
	}

	public void LevelYOffset (float tempYOffset) {
		levelYOffset = tempYOffset;
		for (int i = 0; i < grid2d.GetLength (0); i++) {
			for (int j = 0; j < grid2d.GetLength (1); j++) {
				grid2d [i, j] = 0;
			}
		}
	}

	public void PowerLocalPosition (GameObject powerObject) {
		powerGameObject = powerObject;
		yStart = powerObject.transform.localPosition.y;
		startingPoint = new Vector2 (powerObject.transform.localPosition.x, 0);
		endingPoint = new Vector2 (startingPoint.x, height - 1);
	}

	public void GenerateMapFrombounds (Bounds newBound) {
		// C (0.6, -11.8), E (1.5, 0.4)
		int gridMinY = (int)((-1 * ((Mathf.Round ((newBound.center.y - levelYOffset - yStart - newBound.extents.y) * tileSize) / tileSize)) * tileSize));
		if (gridMinY + 1 >= height * tileSize) {
			return;
		}
		int gridMaxY = (int)((-1 * ((Mathf.Round ((newBound.center.y - levelYOffset - yStart + newBound.extents.y) * tileSize) / tileSize)) * tileSize));
		int gridMinX = (int)((((Mathf.Round ((newBound.center.x - newBound.extents.x) * tileSize) / tileSize) + width / 2) * tileSize) - 1 * playerSize);
		int gridMaxX = (int)((((Mathf.Round ((newBound.center.x + newBound.extents.x) * tileSize) / tileSize) + width / 2) * tileSize) + 1 * playerSize);

		for (int x = gridMinX; x < gridMaxX; x++) {
			if ((x < 0) || (x >= grid2d.GetLength(0))) {
				continue;
			}
			for (int y = gridMaxY; y < gridMinY + 1; y++) {
				if ((y < 0) || (y >= grid2d.GetLength(1))) {
					continue;
				}
				grid2d [x, y] = -1;
			}
		}
	}

	public void FindPath () {
		for (int i = 0; i < grid2d.GetLength (0); i++) {
			for (int j = 0; j < tileSize; j++) {
				grid2d [i, j] = 0;
			}
		}

		Vector2 graphStartingPoint = ScreenToGraphCoordinate (startingPoint.x, startingPoint.y);
		Vector2 graphEndingPoint = ScreenToGraphCoordinate (endingPoint.x, endingPoint.y);
		gameObject.GetComponent<PathFinder> ().FindPath (grid2d, graphStartingPoint, graphEndingPoint);
	}

	public void FinalPath (List<Vector2> graphPath) {
		List<Vector2> screenPath = new List<Vector2> ();
		float lastYValue = 0f;
		foreach (Vector2 graphNode in graphPath) {
			if (graphNode.y != 0 && graphNode.y == lastYValue) {
				screenPath.RemoveAt (screenPath.Count - 1);
			}
			screenPath.Add (GraphToScreenCoordinates ((int)graphNode.x, (int)graphNode.y));
		}
		Debug.Log ("Done");
		powerGameObject.GetComponent<PowerColliderVars> ().pathOfAI = screenPath;
	}

	Vector2 ScreenToGraphCoordinate (float x, float y) {
		Vector2 result;
		result.y = (int)((Mathf.Round (y * tileSize) / tileSize) * tileSize);
		result.x = (int)(((Mathf.Round (x * tileSize) / tileSize) + width / 2) * tileSize);
		return result;
	}

	Vector2 GraphToScreenCoordinates (int x, int y) {
		Vector2 result;
		result.y = (-1 * (y / tileSize)) + yStart + levelYOffset;
		result.x = ((float)x / (float)tileSize) - ((float)width / 2f);
		return result;
	}

	void GridState () {
		for (int i = 0; i < grid2d.GetLength(1); i++) {
			string row = "";
			for (int j = 0; j < grid2d.GetLength(0); j++) {
				row += grid2d[j, i] + " ";
			}
			Debug.Log (row);
		}
	}
}
